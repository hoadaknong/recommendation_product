from urllib.request import urlopen
import json
import numpy as np # linear algebra
import pandas as pd # data processing, CSV file I/O (e.g. pd.read_csv)
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.metrics.pairwise import cosine_similarity



def recommend(barcode):
    url = "http://slayz-api.ap-southeast-1.elasticbeanstalk.com/api/v1/product/product_grid"
    response = urlopen(url)
    data_json = json.loads(response.read())
    
    data_file = data_json['data']

    data_file = list(filter(lambda dictionary: dictionary['inventory'] > 1,data_file))
    df = pd.json_normalize(data_file, max_level = 1)
    
    cv = CountVectorizer(max_features=5000,stop_words='english')
    # vector.shape
    vector = cv.fit_transform(df['title']).toarray()
    similarity = cosine_similarity(vector)
    # similarity
    index = df[df['barcode'] == barcode].index[0]
    distances = sorted(list(enumerate(similarity[index])),reverse=True,key = lambda x: x[1])
    s = []

    for i in distances[1:5]:
        s.append(int(df.iloc[i[0]].productId))
    return s
