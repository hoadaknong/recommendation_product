from flask import Flask, request, jsonify
app = Flask(__name__)
import os
import recommend_AI
from flask_cors import CORS, cross_origin

CORS(app)

@app.route('/get_list_product', methods=['GET'])
def respond():
    # Retrieve the name from the url parameter /getmsg/?name=
    barcode = request.args.get("barcode", None)

    # For debugging
    print(f"Received: {barcode}")

    response = {}

    # Check if the user sent a name at all
    if not barcode:
        response["ERROR"] = "No shoes's name found. Please send a name."
    # Check if the user entered a number
    elif str(barcode).isdigit():
        response["ERROR"] = "The shoes's name can't be numeric. Please send a string."
    else:
        # print(recommend_AI.recommend(name))
        response["result"] = recommend_AI.recommend(barcode)
        # Return the response in json format
        return jsonify(response)

@app.route('/')
def index():
    # A welcome message to test our server
    return "<h1>Welcome to our recommend-AI-api!</h1>"


if __name__ == '__main__':
    # Threaded option to enable multiple instances for multiple user access support
    app.run(threaded=True, port=5001)